# Index




## Convert exisiting .HEIC to .JPG


Single file
`$ convert IMG_3288.HEIC IMG_3288.JPG`

Batch
`$ ls *.HEIC -1 | sed -e 's/\.HEIC$//' | xargs -I {} convert {}.HEIC {}.JPG`


**convert** is part of the ImageMagic application
**libheif** is a library for decoding and ecoding HEIC

## 
