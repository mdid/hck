Create git repo

 `git init --bare $HOME/dotfiles`


Create alias in the .bashrc or fish or zshrc
to use short name for long command that will tell git to use home dir as a snapshot for the bare git repo
 
 
`alias dotgit='git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'`


Tell Git that this repo should not display all untracked files

`dotgit config status.showUntrackedFiles no`

Set up a remote repo to sync dotfiles

`dotgit remote add origin git@gitlab.io:mdid/dotfiles.git`

Commands
```
dotgit add ~/.gitconfig #add config file/folder to repo
dotgit commit -m "Git dotfiles"
dotgit push origin master
```



Reference:

https://stegosaurusdormant.com/bare-git-repo/
https://www.atlassian.com/git/tutorials/dotfiles
 
