# Index
[Change permission on the file](#change-permission-on-the-file)

[Change file ownership](#change file ownership)

[Find files or directories](#find-files-or-directories)

## Change permission on the file

`chmod u=rwx,g=rx,o=r fileName`

This example uses symbolic permissions notation. The letters u, g, and o stand for "user", "group", and "other". The equals sign ("=") means "set the permissions exactly like this," and the letters "r", "w", and "x" stand for "read", "write", and "execute", respectively. The commas separate the different classes of permissions, and there are no spaces between them.

`chmod 754`

Here the digits 7, 5, and 4 each individually represent the permissions for the user, group, and others, in that order. Each digit is a combination of the numbers 4, 2, 1, and 0:

    4 stands for "read",
    2 stands for "write",
    1 stands for "execute", and
    0 stands for "no permission."

So 7 is the combination of permissions 4+2+1 (read, write, and execute), 5 is 4+0+1 (read, no write, and execute), and 4 is 4+0+0 (read, no write, and no execute).

## Change file ownership
`chown -R <user>:<group> <folder_1> <folder_2> ... <folder_n>`

`chown -R user /home/user`

Change the owner of directories and files contained in the home directory of a specific user, you would write

## Find files or directories

`find /home/user -name *.txt`

match all the TXT files in the home directory of the current user.


## Paste file content to terminal

`cat 'file name with space'`


## Do somethig with username/file from the file 

`while IFS= read -r file; do mv "$file" "/media/Data/Quick Review/duplicates"/; done < tbm.txt`


